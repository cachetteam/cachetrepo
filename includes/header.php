<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cachet Qatar</title>
    <link rel="shortcut icon" href="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/style.css">
    </head>

<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="logo_box">
                        <div class="logo">
                            <img src="images/logo.png" alt="Logo">
                        </div>
                    </div>
                </div>
                <div class="col-lg-10 col-md-10 col-sm-10">
                    <div class="top_bar">
                <div class="social_media">
                    <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                    <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
                </div>
                <div class="contact">
                    <a href="tel:04952303819"><span><i class="fa fa-mobile" aria-hidden="true"></i></span>0495 230 3819</a>
                    <a href="tel:04952303819"><span><i class="fa fa-mobile" aria-hidden="true"></i></span>0495 230 3819</a>
                </div>
            </div>
            <div class="down_bar">
                <div class="menu_bar">
                    <nav id="primary_nav_wrap">
                        <ul>
                          <li><a href="#">Home</a></li>
                          <li><a href="#">Products</a>
                            <ul>
                              <li class="dir"><a href="#">For Men</a>
                                <ul>
                                  <li><a href="#">Category 1</a></li>
                                  <li><a href="#">Category 2</a></li>
                                  <li><a href="#">Category 3</a></li>
                                  <li><a href="#">Category 4</a></li>
                                  <li><a href="#">Category 5</a></li>
                                </ul>  
                              </li>
                              <li class="dir"><a href="#">For Women</a>
                                <ul>
                                  <li><a href="#">Category 1</a></li>
                                  <li><a href="#">Category 2</a></li>
                                  <li><a href="#">Category 3</a></li>
                                  <li><a href="#">Category 4</a></li>
                                  <li><a href="#">Category 5</a></li>
                                </ul>
                              </li>
                            </ul>
                          </li>
                          <li><a href="#">Offers</a></li>
                          <li><a href="#">Contact Us</a></li>
                        </ul>
                        </nav>
                </div>
                <div class="features">
                    <span><i class="fa fa-fighter-jet" aria-hidden="true"></i> Delivery with in 1-3 Days</span>
                    <span><i class="fa fa-fighter-jet" aria-hidden="true"></i> 24 Hours Customer Support</span>
                </div>
            </div>
                </div>
            </div>
            
        </div>
        
        <?php if($page_id==1){ ?>
            
        
            <?php } ?>
        
        <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6 || $page_id==7 || $page_id==8 || $page_id==9){ ?>
        
            
            
        
        <?php } ?>
    </header>
    