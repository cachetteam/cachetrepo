<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
<div class="banner">
    <div class="container">
        <div class="search_bx">
            <input type="text" placeholder="Search here...." name="search"><button><i class="fa fa-search" aria-hidden="true"></i></button>
        </div>
        <div id="owl-demo1" class="owl-carousel owl-theme">

          <div class="item">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slide_img">
                    <img src="images/1.jpg" alt="The Last of us">
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slid_description">
                    <h1>Elegante Aviator Men Sunglasses</h1>
                    <span>(AVTGLDGRNMER_59_Green)</span>
                    <p>Elegante' is a premier and renowned brand in sunglasses industry worldwide. Elegante' features innovation, uniqueness and elegantness in their designs and excellence in quality. </p>
                    <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                    <a href="#">Buy now!</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slide_img">
                    <img src="images/1.jpg" alt="The Last of us">
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slid_description">
                    <h1>Elegante Aviator Men Sunglasses</h1>
                    <span>(AVTGLDGRNMER_59_Green)</span>
                    <p>Elegante' is a premier and renowned brand in sunglasses industry worldwide. Elegante' features innovation, uniqueness and elegantness in their designs and excellence in quality. </p>
                    <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                    <a href="#">Buy now!</a>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slide_img">
                    <img src="images/1.jpg" alt="The Last of us">
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="slid_description">
                    <h1>Elegante Aviator Men Sunglasses</h1>
                    <span>(AVTGLDGRNMER_59_Green)</span>
                    <p>Elegante' is a premier and renowned brand in sunglasses industry worldwide. Elegante' features innovation, uniqueness and elegantness in their designs and excellence in quality. </p>
                    <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                    <a href="#">Buy now!</a>
                  </div>
                </div>
              </div>
            </div>

        </div>
    </div>
</div>
   
<div class="offers">
    <div class="heading">
        <div class="container">
            <h1>Offers</h1>
        </div>
        <hr>
    </div>
    <div class="container">
        <div class="product_listing">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box normal">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box normal">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="product_box">
                        <div class="pro_img"><img src="images/pro_2.jpg" alt="Cachet Qatar"></div>
                        <div class="pro_details">
                            <h2>Elegante Aviator Men Sunglasses</h2>
                            <span>(AVTGLDGRNMER_59_Green)</span>
                            <h3 class="old_price">QAR   200</h3><h3 class="new_price">QAR   100</h3>
                            <a href="#"><div class="buy_now">
                                <span><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                                <h4>Buy Now!</h4>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#">View all</a>
        </div>
    </div>
</div>

<div class="buy_easy_steps">
    <div class="container">
        <h1>Buy Easy Steps</h1>
        <p>There are many variations of passages of Lorem Ipsum available, but the majority in some form, by injected of passages ofeven slightly believable.</p>
        <div class="the_prosess">
            <div class="icon_box">
                <span>
                    <img src="images/Forma-1.png" alt="Cachet Qatar">
                </span>
            </div>
            <div class="row_box">
                <span></span>
            </div>
            <div class="icon_box">
                <span>
                    <img src="images/Shape-1.png" alt="Cachet Qatar">
                </span>
            </div>
            <div class="row_box">
                <span></span>
            </div>
            <div class="icon_box">
                <span>
                    <img src="images/Forma-11.png" alt="Cachet Qatar">
                </span>
            </div>
        </div>
    </div>
</div>
    <?php include('includes/footer.php');?>
    <script>
        $(document).ready(function(){
           $("#owl-demo1").owlCarousel({
                      autoPlay : true,
                      navigation : true, // Show next and prev buttons
                      slideSpeed : 300,
                      paginationSpeed : 400,
                      singleItem:true

                      // "singleItem:true" is a shortcut for:
                      // items : 1, 
                      // itemsDesktop : false,
                      // itemsDesktopSmall : false,
                      // itemsTablet: false,
                      // itemsMobile : false

                  }); 
        });
    </script>
        </body>

        </html>